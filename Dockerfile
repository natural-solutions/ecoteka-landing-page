FROM node:current-alpine AS base
WORKDIR /base
COPY package*.json ./
RUN npm install
COPY . .

FROM base AS production
ARG studio_url=https://studio.ecoteka.org
ARG google_analytics=G-TV6ZC362F1
ARG cookie_consent=etk-cookie-consent
ENV NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED 1
ENV STUDIO_URL=${studio_url}
ENV GOOGLE_ANALYTICS=${google_analytics}
ENV COOKIE_CONSENT=${cookie_consent}
ENV NODE_OPTIONS=--openssl-legacy-provider
WORKDIR /production
COPY --from=base /base ./
RUN npm run build

EXPOSE 3000
CMD npm run start