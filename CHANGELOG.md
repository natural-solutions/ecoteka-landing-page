## [1.2.3](https://gitlab.com/natural-solutions/ecoteka-landing-page/compare/v1.2.2...v1.2.3) (2023-02-20)


### Bug Fixes

* NODE_OPTIONS=--openssl-legacy-provider ([b87edd5](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/b87edd534874d02332dd7897e94a200b3e85a865))

## [1.2.2](https://gitlab.com/natural-solutions/ecoteka-landing-page/compare/v1.2.1...v1.2.2) (2021-10-11)


### Bug Fixes

* menu with ([1a8c8bc](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/1a8c8bc614c6b3bea5f16516047e8a860d57a1c6))

## [1.2.1](https://gitlab.com/natural-solutions/ecoteka-landing-page/compare/v1.2.0...v1.2.1) (2021-10-11)


### Bug Fixes

* global gtag type ([5dccb81](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/5dccb81d529f82c5463b358b4fc706a974d22e5d))

# [1.2.0](https://gitlab.com/natural-solutions/ecoteka-landing-page/compare/v1.1.0...v1.2.0) (2021-10-08)


### Bug Fixes

* change index page components responsive font sizes ([30453f7](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/30453f7fdeec7b0f9dffe597a6e9eb03412e9090))
* Fix HTML h tags hierarchy ([b13c670](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/b13c6706b75382c4143598a9d6aa03bcd3eb994a))
* Home page syles fix ([1cdc7f8](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/1cdc7f8971daa44d46c2be115985263cc5dc41bb))
* remove blank space before exclamation point ([49d600f](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/49d600f4a875b9147c861c04626190fbd77f9254))


### Features

* Correctif recette landing page ([f94ad53](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/f94ad53f4da4a81bde0c4710885e1c8bfe01dba7))

# [1.1.0](https://gitlab.com/natural-solutions/ecoteka-landing-page/compare/v1.0.1...v1.1.0) (2021-07-22)


### Features

* add legacy page ([88554bd](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/88554bd5420eb1aa81b11d5b62590fe4270b9805))

## [1.0.1](https://gitlab.com/natural-solutions/ecoteka-landing-page/compare/v1.0.0...v1.0.1) (2021-07-06)


### Bug Fixes

* Change the text button available soon to be replace by discover the app now ([9ed2bef](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/9ed2befdf0d3325d9078564d299a813273e44636))

# 1.0.0 (2021-06-07)


### Bug Fixes

* rename hubButton to HubButton ([5983ef8](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/5983ef84297c4ccdc071a8b22979c7129e062a91))
* rename Subfooter to SubFooter ([0efa89c](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/0efa89c4b65eb116f62d149540e90014aa12a43f))
* update ([b88c4d0](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/b88c4d018e061d67249417d6ef24a16fdb4a0a36))
* **homepage:** Change Homepage based Studio readiness ([f650934](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/f6509345cb4abf74cb293731f5d4bd5c39a35435))
* **homepage:** Change image url for Keys ([9b258c1](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/9b258c1a893200053ca7a60fb1d5ffcdbd0a100b))
* **homepage:** cleanup code + add ENV vars ([e5919fe](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/e5919fea8eb38b0cb352a3693e39e35a3f34f0e5))
* add call to action ([00dc317](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/00dc317f579cef289236e44d06fb22bb91798430))
* add demo images ([6d28d78](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/6d28d782473bcab977ce9ab352ead94d3d65285a))
* add first version footer ([ee95089](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/ee9508902c06dc1ba5ea7e64736656f764c1b836))
* add footer links and translations ([a5fb411](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/a5fb4115792dd684b2ff40498fc40e5c19fa85ec))
* add footer links and translations ([4a70b19](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/4a70b190f3e8dbe346c5137a32216a8337d3be58))
* add home reference ([7782387](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/77823876360636f15ab8f6ce3d2e9e253120b5e4))
* add minor changes ([b119716](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/b119716055e38c09c7ea0eb8e8b6dffc1c76c3bb))
* add section component ([a337e41](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/a337e41c0482a4a802f48728b4011799b1030679))
* added dynamic thumbnails for demo ([232e326](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/232e3260ab907acf940bbf69315a6aac1f45d7b1))


### Features

* add newsletter component ([3f42e2c](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/3f42e2c94f23b5269be263235a53eb02d73d0a76))
* add subfooter component ([8249f9c](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/8249f9c05b67f49e03c3ce1e04388eaac8f68936))
* start project ([8040c41](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/8040c41f43f24cebc5ccc884704760797fcb7203))


### Performance Improvements

* add .env studio_url ([92ffb19](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/92ffb19e94afdd0289898e5212b3332491470662))
* add dark support ([4dc2eda](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/4dc2edab249d0bc3e77a37b713348d9126fa0dda))
* improved hero ([f4b6794](https://gitlab.com/natural-solutions/ecoteka-landing-page/commit/f4b679452b1726cf8ae460d7b3159ca7937f971a))
